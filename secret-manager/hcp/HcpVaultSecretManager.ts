import NodeVault from "node-vault"
import { SecretManager } from "../SecretManager";
import { IHcpVaultSecretManagerConfigs } from "./HcpVaultSecretManagerConfigs";

export class HcpVaultSecretManager implements SecretManager {
  private roleId: string
  private secretId: string
  private vault: NodeVault.client

  constructor(config: IHcpVaultSecretManagerConfigs) {
    this.roleId = config.roleId
    this.secretId = config.secretId

    this.vault = NodeVault({
      apiVersion: config.apiVersion,
      endpoint: config.endpoint,
    })
  }

  private async login(): Promise<void> {
    const result = await this.vault.approleLogin({
      role_id: this.roleId,
      secret_id: this.secretId,
    })

    this.vault.token = result.auth.client_token;
  }

  async getSecrets(key: string): Promise<any> {
    await this.login()
    return this.vault.read(key)
  }
}