import { SecretManagerConfigs } from "../SecretManagerConfig";
import * as path from "path"
import * as dotenv from "dotenv"

dotenv.config({ path: path.join(__dirname, "..", ".env") })

export interface IHcpVaultSecretManagerConfigs {
  apiVersion: string,
  endpoint: string,
  roleId: string,
  secretId: string,
}

export class HcpVaultSecretManagerConfigs implements SecretManagerConfigs {
  static getConfigs(): IHcpVaultSecretManagerConfigs {   
    return {
      apiVersion: process.env.VAULT_API_VERSION,
      endpoint: process.env.VAULT_ADDRESS,
      roleId: process.env.VAULT_ROLE_ID,
      secretId: process.env.VAULT_SECRET_ID,
    }
  }
}