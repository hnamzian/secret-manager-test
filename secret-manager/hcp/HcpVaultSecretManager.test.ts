import { HcpVaultSecretManager } from "./HcpVaultSecretManager";
import { HcpVaultSecretManagerConfigs } from "./HcpVaultSecretManagerConfigs";

async function test() {
  const hcpVaultSecretManagerConfigs = HcpVaultSecretManagerConfigs.getConfigs()

  const hcpVaultSecretManager = new HcpVaultSecretManager(hcpVaultSecretManagerConfigs)

  const data = await hcpVaultSecretManager.getSecrets("secret/data/auth_service")
  console.log(data);
}

test()