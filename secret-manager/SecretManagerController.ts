import { AwsSecretsManager } from "./aws/AwsSecretsManager";
import { GcpSecretManager } from "./gcp/GoogleSecretManager";
import { HcpVaultSecretManager } from "./hcp/HcpVaultSecretManager";
import { SecretManager } from "./SecretManager";

export class SecretManagerController {
  async create(config): Promise<SecretManager> {
    if (config.type === 'hcp') {
      return new HcpVaultSecretManager(config)
    } else if (config.type === 'aws') {
      return new AwsSecretsManager(config)
    } else if (config.type == 'gcp') {
      return new GcpSecretManager(config)
    } else {
      throw new Error("Invalid Secret Manager Type")
    }
  }
}