import { AwsSecretsManagerConfig } from "./aws/AwsSecretsmanagerConfig"
import { GoogleSecretManagerConfigs } from "./gcp/GoogleSecretManagerConfig"
import { HcpVaultSecretManagerConfigs } from "./hcp/HcpVaultSecretManagerConfigs"
import { ISecretManagerConfigs } from "./SecretManagerConfig"

export class SecretManagerConfigController {
  static getConfig(type: string): ISecretManagerConfigs {
    if (type === 'hcp') {
      return HcpVaultSecretManagerConfigs.getConfigs()
    } else if (type === 'aws') {
      return AwsSecretsManagerConfig.getConfigs()
    } else if (type == 'gcp') {
      return GoogleSecretManagerConfigs.getConfigs()
    } else {
      throw new Error("Invalid Secret Manager Type")
    }
  }
}