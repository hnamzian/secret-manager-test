export interface SecretManager {
  getSecrets(secretName: string): Promise<any>
}