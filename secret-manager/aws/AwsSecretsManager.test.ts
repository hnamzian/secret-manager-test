import { AwsSecretsManager } from "./AwsSecretsManager";
import { AwsSecretsManagerConfig } from "./AwsSecretsmanagerConfig";

async function test() {
  const awsSecretsmanagerConfigs = AwsSecretsManagerConfig.getConfigs()
  const awsSecretsManager = new AwsSecretsManager(awsSecretsmanagerConfigs)

  const secret_name = "dev/guardian";
  const secrets = await awsSecretsManager.getSecrets(secret_name)
  console.log(secrets);
}

test()