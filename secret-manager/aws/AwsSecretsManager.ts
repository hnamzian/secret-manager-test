import { SecretsManagerClient, GetSecretValueCommand } from "@aws-sdk/client-secrets-manager"
import { SecretManager } from "../SecretManager";


export interface AwsSecretsManagerConfig {
  region: string,
}

export class AwsSecretsManager implements SecretManager {
  private configs: AwsSecretsManagerConfig

  constructor(configs: AwsSecretsManagerConfig) {
    this.configs = Object.freeze(configs)
  }

  public async getSecrets(secretName: string): Promise<any> {  
    const client = new SecretsManagerClient({
      region: this.configs.region,
    });
    
    let response;
    try {
      response = await client.send(
        new GetSecretValueCommand({
          SecretId: secretName,
          VersionStage: "AWSCURRENT",
        })
      );
    } catch (error) {
      throw error;
    }
    
    const secrets = response.SecretString;
    
    return secrets
  }
}


