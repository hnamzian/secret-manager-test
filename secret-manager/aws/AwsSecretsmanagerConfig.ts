import { SecretManagerConfigs } from "../SecretManagerConfig"
import * as path from "path"
import * as dotenv from "dotenv"

dotenv.config({ path: path.join(__dirname, "..", ".env") })

export interface IAwsSecretManagerConfigs {
  region: string
}

export class AwsSecretsManagerConfig implements SecretManagerConfigs {
  static getConfigs(): IAwsSecretManagerConfigs {
    return {
      region: process.env.AWS_REGION
    }
  }
}