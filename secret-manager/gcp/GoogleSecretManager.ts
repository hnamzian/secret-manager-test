import { SecretManagerServiceClient } from '@google-cloud/secret-manager'
import { SecretManager } from '../SecretManager';
import { IGoogleSecretManagerConfigs } from './GoogleSecretManagerConfig';



export class GcpSecretManager implements SecretManager {
  private client: SecretManagerServiceClient
  private config: IGoogleSecretManagerConfigs

  constructor(config: IGoogleSecretManagerConfigs) {
    this.client = new SecretManagerServiceClient();
    this.config = Object.freeze(config);
  }

  getSecrets = async (secretName: string): Promise<any> => {
    const name = `projects/${this.config.projectId}/secrets/${secretName}/versions/latest`;
    
    const [version] = await this.client.accessSecretVersion({
      name: name,
    });
    
    const payload = version.payload.data.toString();
        
    return payload;
  }

  listSecrets = async () => {
    const [secrets] = await this.client.listSecrets({
      parent: `projects/${this.config.projectId}`,
    });

    secrets.forEach(secret => {
      const policy = secret.replication.userManaged
        ? secret.replication.userManaged
        : secret.replication.automatic;
      console.log(`${secret.name} (${policy})`);
    });

    return secrets
  }
}

