import { SecretManagerConfigs } from "../SecretManagerConfig";
import * as path from "path"
import * as dotenv from "dotenv"

dotenv.config({ path: path.join(__dirname, "..", ".env") })

export interface IGoogleSecretManagerConfigs {
  projectId: string
}

export class GoogleSecretManagerConfigs implements SecretManagerConfigs {
  static getConfigs(): IGoogleSecretManagerConfigs {
    return {
      projectId: process.env.GCP_SM_PROJECT_ID
    }
  }
}