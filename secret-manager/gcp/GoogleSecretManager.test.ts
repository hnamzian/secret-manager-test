import { GcpSecretManager } from "./GoogleSecretManager"
import { GoogleSecretManagerConfigs } from "./GoogleSecretManagerConfig"

async function test() {
  const gcpSecretManagerConfigs = GoogleSecretManagerConfigs.getConfigs()
  const gcpSecretManager = new GcpSecretManager(gcpSecretManagerConfigs)

  const payload = await gcpSecretManager.getSecrets("password")
  console.log(payload)

  const secrets = await gcpSecretManager.listSecrets()
  console.log(secrets)
}

test()