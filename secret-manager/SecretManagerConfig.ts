import { IAwsSecretManagerConfigs } from "./aws/AwsSecretsmanagerConfig";
import { IGoogleSecretManagerConfigs } from "./gcp/GoogleSecretManagerConfig";
import { IHcpVaultSecretManagerConfigs } from "./hcp/HcpVaultSecretManagerConfigs";

export type ISecretManagerConfigs = IAwsSecretManagerConfigs | IGoogleSecretManagerConfigs | IHcpVaultSecretManagerConfigs
export abstract class SecretManagerConfigs {
  static getConfigs(): ISecretManagerConfigs {
    return
  }
}