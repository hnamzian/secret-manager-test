#!/bin/bash

BASE_DIR=./hcp-vault/vault
VAULT_CA_CERT=$BASE_DIR/tls/client/ca.crt
VAULT_CLIENT_CERT=$BASE_DIR/tls/client/client.crt
VAULT_CLIENT_KEY=$BASE_DIR/tls/client/client.key
VAULT_ADDR=https://localhost:8200
VAULT_TOKEN_PATH=$BASE_DIR/data/token
VAULT_TOKEN_FILE_NAME=root
UNSEAL_KEYS_PATH=$BASE_DIR/data/unseal
UNSEAL_KEYS_FILE_NAME=unseal_keys
APPROLE_CREDENTIALS_PATH=$BASE_DIR/data/approle
SECRETS_PATH=$BASE_DIR/data/secrets/secrets.json

declare -a SERVICES=( "api_gateway_service" "auth_service" "guardian_service" "logger_service" "topic_service" "worker_service" )

init_vault() {
  curl -k -s --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --data '{"secret_shares": 5, "secret_threshold": 3}' \
    ${VAULT_ADDR}/v1/sys/init >response.json

  VAULT_TOKEN=$(cat response.json | jq .root_token | tr -d '"')
  UNSEAL_KEYS=$(cat response.json | jq .keys)
  UNSEAL_KEY_0=$(cat response.json | jq .keys | jq '.[0]')
  UNSEAL_KEY_1=$(cat response.json | jq .keys | jq '.[1]')
  UNSEAL_KEY_2=$(cat response.json | jq .keys | jq '.[2]')
  UNSEAL_KEY_3=$(cat response.json | jq .keys | jq '.[3]')

  ERRORS=$(cat response.json | jq .errors | jq '.[0]')
  if [ "$UNSEAL_KEYS" = "null" ]; then
    echo "[VAULT] cannot retrieve unseal key: $ERRORS"
    exit 1
  fi

  if [ -n "$VAULT_TOKEN" ]; then
    echo "[VAULT] Root token saved in ${VAULT_TOKEN_PATH}"
    mkdir -p ${VAULT_TOKEN_PATH}
    echo "$VAULT_TOKEN" >${VAULT_TOKEN_PATH}/${VAULT_TOKEN_FILE_NAME}
  fi

  if [ -n "$UNSEAL_KEYS" ]; then
    echo "[PLUGIN] Unseal_Keys saved in ${UNSEAL_KEYS_PATH}/${UNSEAL_KEYS_FILE_NAME}"
    mkdir -p ${UNSEAL_KEYS_PATH}
    echo "$UNSEAL_KEYS" >${UNSEAL_KEYS_PATH}/${UNSEAL_KEYS_FILE_NAME}
  fi

  rm response.json
}

unseal_vault() {
  curl -k -s --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --data '{"key": '${UNSEAL_KEY_0}'}' \
    ${VAULT_ADDR}/v1/sys/unseal

  curl -k -s --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --data '{"key": '${UNSEAL_KEY_1}'}' \ 
    ${VAULT_ADDR}/v1/sys/unseal

  curl -k -s --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --data '{"key": '${UNSEAL_KEY_2}'}' \
    ${VAULT_ADDR}/v1/sys/unseal

  curl -k -s --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --data '{"key": '${UNSEAL_KEY_3}'}' \
    ${VAULT_ADDR}/v1/sys/unseal
}

enable_kv2_key_engine() {
  curl -k -s --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --header "X-Vault-Token: ${VAULT_TOKEN}" \
    --data '{"type": "kv-v2", "config": {"force_no_cache": true} }' \
    ${VAULT_ADDR}/v1/sys/mounts/secret
}

enable_approle() {
  curl --header "X-Vault-Token: $VAULT_TOKEN" \
    --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --data '{"type": "approle"}' \
    $VAULT_ADDR/v1/sys/auth/approle
}

create_policies() {
  for SERVICE in "${SERVICES[@]}"; do
    POLICY='{
    "policy": "# Read-only permission on secrets stored at '\''secret/data/'$SERVICE''\''\npath \"secret/data/'$SERVICE'\" {\n  capabilities = [ \"read\" ]\n}"
}'
    printf "%s" "$POLICY" > tmp.json
    curl --header "X-Vault-Token: $VAULT_TOKEN" \
      --request PUT \
      --cacert $VAULT_CA_CERT \
      --cert $VAULT_CLIENT_CERT \
      --key $VAULT_CLIENT_KEY \
      --data @tmp.json \
      $VAULT_ADDR/v1/sys/policies/acl/$SERVICE 
  done

  rm tmp.json
}

create_roles() {
  ROLE=$(echo '{}' | jq '.')
  ROLE=$(echo $ROLE | jq '.secret_id_ttl = "10m"')
  ROLE=$(echo $ROLE | jq '.token_num_uses = 1')
  ROLE=$(echo $ROLE | jq '.token_ttl = 20')
  ROLE=$(echo $ROLE | jq '.token_max_ttl = 30')
  ROLE=$(echo $ROLE | jq '.secret_id_num_uses = 4')

  for SERVICE in "${SERVICES[@]}"; do
    ROLE=$(echo $ROLE | jq '.token_policies = "'$SERVICE'"')
    echo $ROLE > tmp.json

    curl --header "X-Vault-Token: $VAULT_TOKEN" \
      --request POST \
      --cacert $VAULT_CA_CERT \
      --cert $VAULT_CLIENT_CERT \
      --key $VAULT_CLIENT_KEY \
      --data @tmp.json \
      $VAULT_ADDR/v1/auth/approle/role/$SERVICE
  done

  rm tmp.json
}

get_role_ids() {
  for SERVICE in "${SERVICES[@]}"; do
    ROLE_ID=$(curl --header "X-Vault-Token: $VAULT_TOKEN" \
      --cacert $VAULT_CA_CERT \
      --cert $VAULT_CLIENT_CERT \
      --key $VAULT_CLIENT_KEY \
      $VAULT_ADDR/v1/auth/approle/role/$SERVICE/role-id | jq -r ".data.role_id")

    echo "ROLE_ID=$ROLE_ID" >> ./creds/$SERVICE.env
  done
}

get_secret_ids() {
  for SERVICE in "${SERVICES[@]}"; do
    SECRET_ID=$(curl --header "X-Vault-Token: $VAULT_TOKEN" \
      --request POST \
      --cacert $VAULT_CA_CERT \
      --cert $VAULT_CLIENT_CERT \
      --key $VAULT_CLIENT_KEY \
      $VAULT_ADDR/v1/auth/approle/role/$SERVICE/secret-id | jq -r ".data.secret_id")
    echo "SECRET_ID=$SECRET_ID" >> ./creds/$SERVICE.env
  done
}

get_approle_credentials() {
  if [ -d $APPROLE_CREDENTIALS_PATH ]; then
    rm -rf $APPROLE_CREDENTIALS_PATH
  fi
  mkdir -p $APPROLE_CREDENTIALS_PATH

  for SERVICE in "${SERVICES[@]}"; do
    ROLE_ID=$(curl --header "X-Vault-Token: $VAULT_TOKEN" \
      --cacert $VAULT_CA_CERT \
      --cert $VAULT_CLIENT_CERT \
      --key $VAULT_CLIENT_KEY \
      $VAULT_ADDR/v1/auth/approle/role/$SERVICE/role-id | jq -r ".data.role_id")

    SECRET_ID=$(curl --header "X-Vault-Token: $VAULT_TOKEN" \
      --request POST \
      --cacert $VAULT_CA_CERT \
      --cert $VAULT_CLIENT_CERT \
      --key $VAULT_CLIENT_KEY \
      $VAULT_ADDR/v1/auth/approle/role/$SERVICE/secret-id | jq -r ".data.secret_id")

    echo "ROLE_ID=$ROLE_ID" >> $APPROLE_CREDENTIALS_PATH/$SERVICE.env
    echo "SECRET_ID=$SECRET_ID" >> $APPROLE_CREDENTIALS_PATH/$SERVICE.env
  done
}

push_secrets() {
  SECRETS=$(cat $SECRETS_PATH)

  for SERVICE in "${SERVICES[@]}"; do
    if jq -e '.'$SERVICE' != null' $SECRETS_PATH > /dev/null ; then
      KeyValues=$(echo $SECRETS | jq '.'$SERVICE'')

      curl --header "X-Vault-Token: $VAULT_TOKEN" \
          --request POST \
          --cacert $VAULT_CA_CERT \
          --cert $VAULT_CLIENT_CERT \
          --key $VAULT_CLIENT_KEY \
          --data "{\"data\": $KeyValues}" \
          $VAULT_ADDR/v1/secret/data/$SERVICE
    fi
  done
}

echo "[Vault] Initializing Vault: ${VAULT_ADDR}"
init_vault

echo "[Vault] Unsealing vault..."
unseal_vault

echo "[Vault] Enable KV2 Key engine"
enable_kv2_key_engine

echo "[VAULT] Enable AppRole"
enable_approle

echo "[VAULT] Create Policies"
create_policies

echo "[VAULT] Create Roles"
create_roles

echo "[VAULT] Get AppRole Credentials"
get_approle_credentials

echo "[VAULT] Push Secrets into Vault"
push_secrets

exit 0
