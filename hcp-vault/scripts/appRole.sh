VAULT_CA_CERT=$PWD/hcp-vault/vault/tls/client/ca.crt
VAULT_CLIENT_CERT=$PWD/hcp-vault/vault/tls/client/client.crt
VAULT_CLIENT_KEY=$PWD/hcp-vault/vault/tls/client/client.key 
VAULT_ADDR=https://localhost:8200
VAULT_TOKEN=s.7iNfEb3WsVnxDD67ViWl8FlE



# curl --header "X-Vault-Token: $VAULT_TOKEN" \
#     --cacert $VAULT_CA_CERT \
#     --cert $VAULT_CLIENT_CERT \
#     --key $VAULT_CLIENT_KEY \
#     --request POST \
#     --data '{"type": "approle"}' \
#     $VAULT_ADDR/v1/sys/auth/approle


# vault auth list \
#     -ca-cert=$VAULT_CA_CERT \
#     -client-cert=$VAULT_CLIENT_CERT \
#     -client-key=$VAULT_CLIENT_KEY

# vault auth enable \
#     -ca-cert=$VAULT_CA_CERT \
#     -client-cert=$VAULT_CLIENT_CERT \
#     -client-key=$VAULT_CLIENT_KEY\
#     approle 

# vault write \
#     -ca-cert=$VAULT_CA_CERT \
#     -client-cert=$VAULT_CLIENT_CERT \
#     -client-key=$VAULT_CLIENT_KEY\
#     auth/approle/role/jenkins \
#     token_policies="jenkins" \
#     secret_id_ttl=10m \
#     token_num_uses=10 \
#     token_ttl=20m \
#     token_max_ttl=30m \
#     secret_id_num_uses=40


# vault read \
#     -ca-cert=$VAULT_CA_CERT \
#     -client-cert=$VAULT_CLIENT_CERT \
#     -client-key=$VAULT_CLIENT_KEY\
#     auth/approle/role/jenkins/role-id

AUTH_SERVICE_ROLE_ID=800cf2b4-f734-5ba3-8f3e-2f8219e4281f

AUTH_SERVICE_SECRET_ID=$(curl -s --header "X-Vault-Token: $VAULT_TOKEN" \
      --request POST \
      --cacert $VAULT_CA_CERT \
      --cert $VAULT_CLIENT_CERT \
      --key $VAULT_CLIENT_KEY \
      $VAULT_ADDR/v1/auth/approle/role/auth_service/secret-id | jq -r ".data.secret_id")
echo $AUTH_SERVICE_SECRET_ID


APP_TOKEN=$(curl -s --request POST \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    --data '{"role_id": "'$AUTH_SERVICE_ROLE_ID'", "secret_id": "'$AUTH_SERVICE_SECRET_ID'"}' \
    $VAULT_ADDR/v1/auth/approle/login | jq -r ".auth.client_token")
echo $APP_TOKEN

curl -s \
    -H "X-Vault-Token: $APP_TOKEN" \
    --cacert $VAULT_CA_CERT \
    --cert $VAULT_CLIENT_CERT \
    --key $VAULT_CLIENT_KEY \
    $VAULT_ADDR/v1/secret/data/auth_service | jq -r ".data.data"


# vault write \
#     -ca-cert=$VAULT_CA_CERT \
#     -client-cert=$VAULT_CLIENT_CERT \
#     -client-key=$VAULT_CLIENT_KEY\
#     auth/approle/login \
#     role_id=$AUTH_SERVICE_ROLE_ID \
#     secret_id=$AUTH_SERVICE_SECRET_ID

# VAULT_TOKEN=$APP_TOKEN vault kv put \
#     -ca-cert=$VAULT_CA_CERT \
#     -client-cert=$VAULT_CLIENT_CERT \
#     -client-key=$VAULT_CLIENT_KEY\
#     secret/mysql/webapp \
#     pwd=123

# VAULT_TOKEN=$APP_TOKEN vault kv get \
#     -ca-cert=$VAULT_CA_CERT \
#     -client-cert=$VAULT_CLIENT_CERT \
#     -client-key=$VAULT_CLIENT_KEY\
#     secret/auth_service

