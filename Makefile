build:
	npx tsc
	cp ./secret-manager/.env dist/.env

build-aws:
	npx tsc ./aws/*.ts --outDir dist/aws
test-aws:
	node dist/aws/AwsSecretsManager.test.js

build-azure:
	npx tsc ./azure/*.ts --outDir dist/azure

build-gcp:
	npx tsc ./gcp/*.ts --outDir dist/gcp
test-gcp:
	node dist/gcp/GoogleSecretManager.test.js

build-hcp:
	npx tsc ./hcp/*.ts --outDir dist/hcp
test-hcp:
	node dist/hcp/HcpVaultSecretManager.test.js

hcp-keygen:
	./hcp-vault/scripts/keygen.sh

hcp-clean-keys:
	rm -rf ./hcp-vault/vault/tls
	rm -rf ./hcp-vault/consul/tls

hcp-up:
	docker-compose -f ./hcp-vault/docker-compose.yaml up -d
	sleep 10
	./hcp-vault/scripts/vault_init.sh

hcp-down:
	docker-compose -f ./hcp-vault/docker-compose.yaml down -v
	rm -rf ./hcp-vault/vault/data/approle
	rm -rf ./hcp-vault/vault/data/token
	rm -rf ./hcp-vault/vault/data/unseal
	sudo rm -rf ./hcp-vault/vault/tls
	sudo rm -rf ./hcp-vault/consul/tls
	sudo rm -rf ./hcp-vault/vault/repository
	sudo rm -rf ./hcp-vault/consul/data